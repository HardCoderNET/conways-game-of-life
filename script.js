var running = false
var living = new Array(50);
for (var i = 0; i < 50; i++) {
	living[i] = new Array(50);
	for (j = 0; j < 50; j++) {
		living[i][j] = false;
	}
}

function main() {
	drawBackground();
}

function drawBackground() {
	var canvas = document.getElementById('CGoL');
	if (canvas.getContext) {
		var ctx = canvas.getContext('2d');


		ctx.beginPath();
		for (i = 0; i < 50; i = i + 1) {
			ctx.moveTo(18 * i - 0.5, 0)
			ctx.lineTo(18 * i - 0.5, 900)
			ctx.moveTo(0, 18 * i - 0.5)
			ctx.lineTo(900, 18 * i - 0.5)
		}
		ctx.stroke();
		canvas.addEventListener('mousedown', click, false);
	}
}

function click(event) {
	var x = new Number();
	var y = new Number();
	var canvas = document.getElementById('CGoL');
	var ctx = canvas.getContext('2d');

	if (event.x != undefined && event.y != undefined) {
		x = event.pageX;
		y = event.pageY;
	} else {
		x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	x -= canvas.offsetLeft;
	y -= canvas.offsetTop;

	x = Math.floor(x / 18)
	y = Math.floor(y / 18)

	if (living[x][y]) {
		clearCell(x, y);
	} else {
		fillCell(x, y);
	}
}

function fillCell(x, y) {
	var canvas = document.getElementById('CGoL');
	var ctx = canvas.getContext('2d');
	ctx.fillRect(x * 18, y * 18, 18, 18)
	living[x][y] = true
}

function clearCell(x, y) {
	var canvas = document.getElementById('CGoL');
	var ctx = canvas.getContext('2d');
	ctx.clearRect(x * 18, y * 18, 18 - 1, 18 - 1)
	living[x][y] = false
}

function calculateLivingNeighbors() {
	var livingNeighbors = new Array(50);
	for (var i = 0; i < 50; i++) {
		livingNeighbors[i] = new Array(50);
		for (j = 0; j < 50; j++) {
			livingNeighbors[i][j] = 0;
		}
	}
	for (i = 0; i < 50; i++) {
		for (j = 0; j < 50; j++) {
			if ((i != 0) && (j != 0) && (i != 49) && (j != 49)) {
				for (m = -1; m < 2; m++) {
					for (n = -1; n < 2; n++) {
						if (living[i + m][j + n]) {
							livingNeighbors[i][j] += 1;
						}
					}
				}
				if (living[i][j]) {
					livingNeighbors[i][j]--;
				}
			} else {
				if ((i == 0) && (j == 0)) {
					if (living[1][0]) {
						livingNeighbors[0][0]++;
					}
					if (living[0][1]) {
						livingNeighbors[0][0]++;
					}
					if (living[1][1]) {
						livingNeighbors[0][0]++;
					}
				} else {

					if ((i == 0) && (j == 49)) {
						if (living[0][48]) {
							livingNeighbors[0][49]++;
						}
						if (living[1][49]) {
							livingNeighbors[0][49]++;
						}
						if (living[1][48]) {
							livingNeighbors[0][49]++;
						}
					} else {

						if ((i == 49) && (j == 0)) {
							if (living[48][0]) {
								livingNeighbors[49][0]++;
							}
							if (living[48][1]) {
								livingNeighbors[49][0]++;
							}
							if (living[49][1]) {
								livingNeighbors[49][0]++;
							}
						} else {
							if ((i == 49) && (j == 49)) {
								if (living[48][49]) {
									livingNeighbors[49][49]++;
								}
								if (living[48][48]) {
									livingNeighbors[49][49]++;
								}
								if (living[49][48]) {
									livingNeighbors[49][49]++;
								}
							} else {
								if (i == 0) {
									for (m = 0; m < 2; m++) {
										for (n = -1; n < 2; n++) {
											if (living[i + m][j + n]) {
												livingNeighbors[i][j] += 1;
											}
										}
									}
									if (living[i][j]) {
										livingNeighbors[i][j]--;
									}
								}
								if (i == 49) {
									for (m = -1; m < 1; m++) {
										for (n = -1; n < 2; n++) {
											if (living[i + m][j + n]) {
												livingNeighbors[i][j] += 1;
											}
										}
									}
									if (living[i][j]) {
										livingNeighbors[i][j]--;
									}
								}
								if (j == 0) {
									for (m = -1; m < 2; m++) {
										for (n = 0; n < 2; n++) {
											if (living[i + m][j + n]) {
												livingNeighbors[i][j] += 1;
											}
										}
									}
									if (living[i][j]) {
										livingNeighbors[i][j]--;
									}
								}
								if (j == 49) {
									for (m = -1; m < 2; m++) {
										for (n = -1; n < 1; n++) {
											if (living[i + m][j + n]) {
												livingNeighbors[i][j] += 1;
											}
										}
									}
									if (living[i][j]) {
										livingNeighbors[i][j]--;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return livingNeighbors;
}

function hitStart(sender) {
	console.log(sender);
	running = !running;
	if (running)
	{
		sender.innerHTML = 'Stop';
	}else{
		sender.innerHTML = 'Start';
	}
	runGame();
}

function runGame() {
	if (running) {
		currentLivingNeighbors = calculateLivingNeighbors();
		for (i = 0; i < 50; i++) {
			for (j = 0; j < 50; j++) {
				if (living[i][j]) {

					if (currentLivingNeighbors[i][j] < 2) {
						clearCell(i, j);
					}
					if ((currentLivingNeighbors[i][j] == 2) || (currentLivingNeighbors[i][j] == 3)) {
						fillCell(i, j);
					}
					if (currentLivingNeighbors[i][j] > 3) {
						clearCell(i, j)
					}
				} else {
					if (currentLivingNeighbors[i][j] == 3) {
						fillCell(i, j);
					}
				}
			}
		}
		setTimeout("runGame()", 300)
	}
}